$(document).ready(function() {
    //ancor con attributo data attribute
    $('a[data-connect]').click(function() {
        var i = $(this).find('i');
        //cambia le icon delle frecce
        i.hasClass('fas fa-arrow-down') ? i.removeClass('fas fa-arrow-down').addClass('fas fa-arrow-up') : i.removeClass('fas fa-arrow-up').addClass('fas fa-arrow-down');
        //slideToggle fa slideUp() e slideDown() 
        $(this).parent().parent().next().slideToggle(); //animazione di comparsa di tutto quello che è nel topic
    });
});