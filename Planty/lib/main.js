function inizializzaLogin() {

    //Questa prima parte riguarda lo scroll top
    ///////////////////////////////
    var mybutton = document.getElementById("myTop");
    mybutton.style.display = "none";

    // Quando scrolli di 30 px dal top del documento o del body show del bottone
    window.onscroll = function() {
        scrollFunction()
    };

    function scrollFunction() {
        if (document.body.scrollTop > 30 || document.documentElement.scrollTop > 30) {
            mybutton.style.display = "block";
        } else {
            mybutton.style.display = "none";
        }
    }
    /////////////////FORM//////////////////

    function accedi(element) { //element=div che contiene le form 
        this.element = element;
        this.blocks = this.element.getElementsByClassName('js-js-accedi-block'); //login registrati e resetta
        this.switchers = this.element.getElementsByClassName('js-js-accedi-switcher')[0].getElementsByTagName('a');
        this.triggers = document.getElementsByClassName('js-js-accedi-trigger');
        this.hidePassword = this.element.getElementsByClassName('js-hide-password');
        this.init();
    };

    accedi.prototype.init = function() {
        var self = this;
        //aggiunge i listener per i bottoni trigger e chiama la funzione accediForm passandogli la sezione richiesta da aprire
        //apri il modulo/switch la form (da registrazione a login o viceversa)
        for (var i = 0; i < this.triggers.length; i++) {
            (function(i) {
                self.triggers[i].addEventListener('click', function(event) {
                    if (event.target.hasAttribute('data-accedi')) { //data accedi contiene l'elemento che clicchi es. login
                        event.preventDefault();
                        self.accediForm(event.target.getAttribute('data-accedi'));
                    }
                });
            })(i);
        }

        //chiudi tutto il modulo 
        this.element.addEventListener('click', function(event) {
            if (hasClass(event.target, 'js-js-accedi')) { //classe del div di tutta la form 
                event.preventDefault();
                removeClass(self.element, 'form--is-visible'); //definita nel css la mette a visible
            }
        });

        //setta il listener per nascondere la password
        for (var i = 0; i < this.hidePassword.length; i++) {
            (function(i) {
                self.hidePassword[i].addEventListener('click', function(event) {
                    self.togglePassword(self.hidePassword[i]);
                });
            })(i);
        }
    };

    //Nasconde e mostra la password cambiando l'attributo type in text o in password 
    accedi.prototype.togglePassword = function(target) {
        var password = target.previousElementSibling; //l'elemento vicino al bottone è la zona text della password
        ('password' == password.getAttribute('type')) ? password.setAttribute('type', 'text'): password.setAttribute('type', 'password');
        target.textContent = ('Nascondi' == target.textContent) ? 'Mostra' : 'Nascondi';
    }

    accedi.prototype.accediForm = function(type) {
        // mostra il modulo se non é visibile
        !hasClass(this.element, 'form--is-visible') && addClass(this.element, 'form--is-visible');
        // mostra la form selezionata 
        for (var i = 0; i < this.blocks.length; i++) {
            this.blocks[i].getAttribute('data-type') == type ? addClass(this.blocks[i], 'form__block--is-selected') : removeClass(this.blocks[i], 'form__block--is-selected');
        }
        //update switcher appearance (aggiorna l'aspetto del commutatore)
        var switcherType = (type == 'signup') ? 'signup' : 'login';
        for (var i = 0; i < this.switchers.length; i++) {
            this.switchers[i].getAttribute('data-type') == switcherType ? addClass(this.switchers[i], 'cd-selected') : removeClass(this.switchers[i], 'cd-selected');
        }
    };

    //Inizializza tutto, passa la div contente tutta la form alla prima funzione
    var accediModal = document.getElementsByClassName("js-js-accedi")[0];
    if (accediModal) {
        new accedi(accediModal);
    }

    //In termini di ereditarietà, Javascript ha solo un costrutto: gli oggetti. Ogni oggetto ha un link interno ad un altro 
    //oggetto chiamato prototype. Questo oggetto prototype 
    //ha a sua volta un suo prototype, e così via finché si raggiunge un oggetto con property null. null, per definizione,
    //non ha un prototype, ed agisce come link finale nella catena di prototipi.
    //Quasi tutti gli oggetti in Javascript sono istanze di Object, che risiede in cima alla catena dei prototipi.
    //Gli oggetti javaScript sono "contenitori" dinamici di proprietà (own properties). Gli oggetti JavaScript 
    //hanno un link ad un oggetto prototype.

    /////////////////FINE FORM//////////////////

    //NECESSARIE se non funzionano le modifiche delle classi nativamente 
    function hasClass(el, className) {
        if (el.classList) return el.classList.contains(className);
        else return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
    }

    function addClass(el, className) {
        var classList = className.split(' ');
        if (el.classList) el.classList.add(classList[0]);
        else if (!hasClass(el, classList[0])) el.className += " " + classList[0];
        if (classList.length > 1) addClass(el, classList.slice(1).join(' '));
    }

    function removeClass(el, className) {
        var classList = className.split(' ');
        if (el.classList) el.classList.remove(classList[0]);
        else if (hasClass(el, classList[0])) {
            var reg = new RegExp('(\\s|^)' + classList[0] + '(\\s|$)');
            el.className = el.className.replace(reg, ' ');
        }
        if (classList.length > 1) removeClass(el, classList.slice(1).join(' '));
    }

    function toggleClass(el, className, bool) {
        if (bool) addClass(el, className);
        else removeClass(el, className);
    }
};

///FUNZIONE PER LA FORM 
var myInput = document.getElementById("signup-password");
var letter = document.getElementById("letter");
var capital = document.getElementById("capital");
var number = document.getElementById("number");
var length = document.getElementById("length");

//User clicca
myInput.onfocus = function() {
    document.getElementById("message").style.display = "block";
}

//sulla perdita di focus
myInput.onblur = function() {
    document.getElementById("message").style.display = "none";
}

// Quando l'utente clicca/premi un tasto e si alza(=onkeyup) sul campo di testo della password
myInput.onkeyup = function() {
    // minuscole
    var lowerCaseLetters = /[a-z]/g; //la g é un flag che fa si che tutte le espressioni regolari che matchano con l'espressioni regolari siano ritornate
    if (myInput.value.match(lowerCaseLetters)) {
        letter.classList.remove("invalid");
        letter.classList.add("valid");
    } else {
        letter.classList.remove("valid");
        letter.classList.add("invalid");
    }

    //maiuscole
    var upperCaseLetters = /[A-Z]/g;
    if (myInput.value.match(upperCaseLetters)) {
        capital.classList.remove("invalid");
        capital.classList.add("valid");
    } else {
        capital.classList.remove("valid");
        capital.classList.add("invalid");
    }

    //numeri
    var numbers = /[0-9]/g;
    if (myInput.value.match(numbers)) {
        number.classList.remove("invalid");
        number.classList.add("valid");
    } else {
        number.classList.remove("valid");
        number.classList.add("invalid");
    }
}