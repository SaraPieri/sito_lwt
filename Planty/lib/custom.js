$(document).ready(function() {
    //animazioni al click del bottone per scrolltop lento
    $("#myTop").on('click', animazione);
    $(".btnn").on('click', animazione2);
    $(".chi").on('click', animazione2);
    $(".feat").on('click', animazione3);

    //////animazione scrolltop lento 
    function animazione() {
        $('html, body').animate({ scrollTop: 0 }, '30'); //inzio torna al pixel 0 velocità 30
    };

    function animazione2() {
        $('html, body').animate({ scrollTop: 700 }, '1'); //sezione chi siamo 
    }

    function animazione3() {
        $('html, body').animate({ scrollTop: 1900 }, '1') //sezione come inziare
    }
    ///////

    $('[data-toggle="counter-up"]').counterUp({
        delay: 30,
        time: 2000
    });

    //per far si che quanto pigi funziona la barra del menù a tendina per smartphone
    $(".header__icon-bar ").click(function(e) {
        $(".header__menu ").toggleClass('is-open'); //con toggleClass vado ad aggiungere una classe all'elemento(in questo caso al menu)
        e.preventDefault();
    });

    // Stick dell'header in cima al menu
    $(".header ").sticky({
        topSpacing: 0,
        zIndex: '40'
    });

    //Set dell'orologio
    /*
    $(function() {

        setInterval(function() {
            var seconds = new Date().getSeconds();
            var sdegree = seconds * 6;
            var srotate = "rotate(" + sdegree + "deg)";

            $("#sec").css({ "transform": srotate });

        }, 1000);

        setInterval(function() {
            var hours = new Date().getHours();
            var mins = new Date().getMinutes();
            var hdegree = hours * 30 + (mins / 2);
            var hrotate = "rotate(" + hdegree + "deg)";

            $("#hour").css({ "transform": hrotate });

        }, 1000);

        setInterval(function() {
            var mins = new Date().getMinutes();
            var mdegree = mins * 6;
            var mrotate = "rotate(" + mdegree + "deg)";

            $("#min").css({ "transform": mrotate });

        }, 1000);

    });*/
});