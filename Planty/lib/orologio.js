//Funzione grafica per le sincronizzazione della lancetta dell'ora
(function createSecondLines() {
    var clock = document.querySelector(".clock");
    var rotate = 0;

    var byFive = function(n) {
        return (n / 5 === parseInt(n / 5, 10)) ? true : false;
    };

    for (i = 0; i < 30; i++) {
        var span = document.createElement("span");

        if (byFive(i)) {
            span.className = "fives";
        }

        span.style.transform = "translate(-50%,-50%) rotate(" + rotate + "deg)";
        clock.appendChild(span);
        rotate += 6;
    }
})();

(function setClock() {
    var time = new Date();
    //legge l'orario usando date
    var hours = time.getHours();
    var minutes = time.getMinutes();
    var seconds = time.getSeconds();

    //crea oggetto clock e salva elementi 
    //ritorna il primo element del documento con selettore 
    var clock = {
        hours: document.querySelector('.hours'),
        minutes: document.querySelector('.minutes'),
        seconds: document.querySelector('.seconds')
    };
    //calcola i gradi di cui devono girare le lancette 
    var deg = {
            hours: 30 * hours + .5 * minutes,
            minutes: 6 * minutes + .1 * seconds,
            seconds: 6 * seconds
        }
        //imposta le lancette al primo caricamneto della pagina
    clock.hours.style.transform = 'rotate(' + deg.hours + 'deg)';
    clock.minutes.style.transform = 'rotate(' + deg.minutes + 'deg)';
    clock.seconds.style.transform = 'rotate(' + deg.seconds + 'deg)';

    var runClock = function() {
        deg.hours += 360 / 43200; //sec per 12ore
        deg.minutes += 360 / 3600; //sec per ora
        deg.seconds += 360 / 60; //sec per minuto

        clock.hours.style.transform = 'rotate(' + deg.hours + 'deg)';
        clock.minutes.style.transform = 'rotate(' + deg.minutes + 'deg)';
        clock.seconds.style.transform = 'rotate(' + deg.seconds + 'deg)';
    };
    //ogni secondo=1000ms chiama un timer e ruota l'orologio di 
    setInterval(runClock, 1000);

    (function printDate() {
        var months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        var print = time.getDate() + ' / ' + months[time.getMonth()];
        var output = document.querySelectorAll('output'); //output è il contenitore della data

        [].forEach.call(output, function(node) { //sostituisce il node con output
            //chiama la f su ogni elem dell'array
            node.innerHTML = print;
        });
    })();
})();