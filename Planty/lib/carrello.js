var count = 0;
var cuori = 0;

if (localStorage.getItem('cuore')) {
    cuori = JSON.parse(localStorage.getItem('cuore'));
    document.getElementById("cuore").innerHTML = cuori;
}
if (localStorage.getItem('conto')) {
    count = JSON.parse(localStorage.getItem('conto'));
}

Vue.component("item", {
    template: "#product-box",
    props: ["item_data", "buyitems"],
    methods: {
        addItem: function(item_data) {
            count += 1;
            item_data.num++;
            if (item_data.num <= 1) {
                this.pushData();
            } else {
                var i = this.findIndex(this.$parent.buyitems, "title", item_data.title);
                this.$parent.buyitems[i].qty += 1;
                this.$parent.buyitems[i].total = this.$parent.buyitems[i].qty * this.$parent.buyitems[i].price;
                console.log(i);
            }
        },
        pushData: function() {
            this.$parent.buyitems.push({
                img: this.item_data.img,
                title: this.item_data.title,
                price: this.item_data.price,
                qty: 1,
                total: this.item_data.price,
                id: this.item_data.id,
                num: this.item_data.num
            });
        },
        findIndex: function(array, attr, value) {
            for (var i = 0; i < array.length; i += 1) {
                if (array[i][attr] === value) {
                    return i;
                }
            }
            return -1;
        },
    }
});
Vue.component("buyitem", {
    template: "#buy-box",
    props: ["items", "buy_data", "buyitems"],
    methods: {
        removeItem: function(buy_data) {
            var index = this.$parent.buyitems.indexOf(buy_data);
            this.$parent.buyitems.splice(index, 1);
            var j = this.trovaIndice(this.$parent.items, "title", buy_data.title);
            this.$parent.items[j].num = 0;
            count -= buy_data.qty;
        },
        plusQty: function(buy_data) {
            count++;
            buy_data.qty += 1;
            buy_data.total = (buy_data.qty * buy_data.price).toFixed(2);
            localStorage.setItem('buyitems', JSON.stringify(this.$parent.buyitems));
            localStorage.setItem('conto', JSON.stringify(count));
        },
        minusQty: function(buy_data) {
            count--;

            buy_data.qty -= 1;
            if (buy_data.qty < 0) {
                buy_data.qty = 0;
            }
            if (buy_data.qty == 0) {
                this.removeItem(buy_data);
            }
            buy_data.total = (buy_data.qty * buy_data.price).toFixed(2);

            localStorage.setItem('buyitems', JSON.stringify(this.$parent.buyitems));
            localStorage.setItem('conto', JSON.stringify(count));

        },

        trovaIndice: function(array, attr, value) {
            for (var i = 0; i < array.length; i += 1) {
                if (array[i][attr] === value) {
                    return i;
                }
            }
            return -1;
        },

    }
});

var app = new Vue({
    el: "#app",
    data: {
        items: [{
                num: 0,
                like: 0,
                vaso: false,
                utile: false,
                altro: false,
                pianta: true,
                img: "Foto_shop/Piante/banano.jpg",
                title: "Pianta di banane",
                price: "12.00",
                id: 0,
                foto_dim: {
                    'background-image': `url('Foto_shop/Piante/banano.jpg')`
                }

            }, {
                num: 0,
                like: 0,
                vaso: true,
                utile: false,
                altro: false,
                pianta: false,
                img: "Foto_shop/Vasi/vaso7.jpg",
                title: "Vaso in legno",
                price: "6.00",
                id: 1,
                foto_dim: {
                    'background-image': `url('Foto_shop/Vasi/vaso7.jpg')`
                }

            }, {
                num: 0,
                like: 0,
                vaso: false,
                utile: true,
                altro: false,
                pianta: false,
                img: "Foto_shop/Utili/guanti.jpg",
                title: "Guanti da giardinaggio",
                price: "15.00",
                id: 2,
                foto_dim: {
                    'background-image': `url('Foto_shop/Utili/guanti.jpg')`
                }

            }, {
                num: 0,
                like: 0,
                vaso: true,
                utile: false,
                altro: false,
                pianta: false,
                img: "Foto_shop/Vasi/vaso1.jpg",
                title: "Vaso in pietra",
                price: "25.00",
                id: 3,
                foto_dim: {
                    'background-image': `url('Foto_shop/Vasi/vaso1.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: false,
                altro: false,
                pianta: true,
                img: "Foto_shop/Piante/cocco.jpg",
                title: "Pianta di cocco",
                price: "10.00",
                id: 4,
                foto_dim: {
                    'background-image': `url('Foto_shop/Piante/cocco.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: false,
                altro: false,
                pianta: true,
                img: "Foto_shop/Piante/cacao.jpg",
                title: "Pianta di cacao",
                price: "15.00",
                id: 5,
                foto_dim: {
                    'background-image': `url('Foto_shop/Piante/cacao.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: true,
                utile: false,
                altro: false,
                pianta: false,
                img: "Foto_shop/Vasi/vaso4.jpg",
                title: "Vaso da interno",
                price: "20.00",
                id: 6,
                foto_dim: {
                    'background-image': `url('Foto_shop/Vasi/vaso4.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: true,
                altro: false,
                pianta: false,
                img: "Foto_shop/Utili/griembiuli.jpg",
                title: "Set di grembiuli",
                price: "10.00",
                id: 7,
                foto_dim: {
                    'background-image': `url('Foto_shop/Utili/griembiuli.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: false,
                altro: false,
                pianta: true,
                img: "Foto_shop/Piante/carota.jpg",
                title: "Pianta di carota",
                price: "10.50",
                id: 8,
                foto_dim: {
                    'background-image': `url('Foto_shop/Piante/carota.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: true,
                altro: false,
                pianta: false,
                img: "Foto_shop/Utili/stivali.jpg",
                title: "Stivali da giardino",
                price: "17.00",
                id: 9,
                foto_dim: {
                    'background-image': `url('Foto_shop/Utili/stivali.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: false,
                altro: false,
                pianta: true,
                img: "Foto_shop/Piante/caffe.jpg",
                title: "Pianta di caffè",
                price: "9.50",
                id: 10,
                foto_dim: {
                    'background-image': `url('Foto_shop/Piante/caffe.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: false,
                altro: false,
                pianta: true,
                img: "Foto_shop/Piante/pistacchio.jpg",
                title: "Pianta di pistacchi",
                price: "20.00",
                id: 11,
                foto_dim: {
                    'background-image': `url('Foto_shop/Piante/pistacchio.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: true,
                altro: false,
                pianta: false,
                img: "Foto_shop/Utili/nomi.jpg",
                title: "Set nomi piante in legno",
                price: "2.00",
                id: 12,
                foto_dim: {
                    'background-image': `url('Foto_shop/Utili/nomi.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: false,
                altro: false,
                pianta: true,
                img: "Foto_shop/Piante/limone.jpg",
                title: "Pianta di limone",
                price: "8.00",
                id: 13,
                foto_dim: {
                    'background-image': `url('Foto_shop/Piante/limone.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: false,
                altro: true,
                pianta: false,
                img: "Foto_shop/Utili/terra1.jpg",
                title: "Terriccio universale",
                price: "20.00",
                id: 14,
                foto_dim: {
                    'background-image': `url('Foto_shop/Utili/terra1.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: false,
                altro: false,
                pianta: true,
                img: "Foto_shop/Piante/patata.jpg",
                title: "Pianta di patate",
                price: "8.00",
                id: 15,
                foto_dim: {
                    'background-image': `url('Foto_shop/Piante/patata.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: true,
                utile: false,
                altro: false,
                pianta: false,
                img: "Foto_shop/Vasi/vaso2.jpg",
                title: "Set vasi appendibili",
                price: "15.00",
                id: 16,
                foto_dim: {
                    'background-image': `url('Foto_shop/Vasi/vaso2.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: false,
                altro: false,
                pianta: true,
                img: "Foto_shop/Piante/albicocca.jpg",
                title: "Pianta di albicocche",
                price: "12.50",
                id: 17,
                foto_dim: {
                    'background-image': `url('Foto_shop/Piante/albicocca.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: true,
                altro: false,
                pianta: false,
                img: "Foto_shop/Utili/attrezzo2.jpg",
                title: "Vanghetta",
                price: "8.00",
                id: 18,
                foto_dim: {
                    'background-image': `url('Foto_shop/Utili/attrezzo2.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: true,
                utile: false,
                altro: false,
                pianta: false,
                img: "Foto_shop/Vasi/vaso10.jpg",
                title: "Vaso di ceramica",
                price: "9.00",
                id: 19,
                foto_dim: {
                    'background-image': `url('Foto_shop/Vasi/vaso10.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: false,
                altro: false,
                pianta: true,
                img: "Foto_shop/Piante/anice.jpg",
                title: "Anice",
                price: "12.00",
                id: 20,
                foto_dim: {
                    'background-image': `url('Foto_shop/Piante/anice.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: true,
                utile: false,
                altro: false,
                pianta: false,
                img: "Foto_shop/Vasi/vaso9.jpg",
                title: "Vaso elegante",
                price: "16.00",
                id: 21,
                foto_dim: {
                    'background-image': `url('Foto_shop/Vasi/vaso9.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: false,
                altro: false,
                pianta: true,
                img: "Foto_shop/Piante/avocado.jpg",
                title: "Pianta di avocado",
                price: "13.00",
                id: 22,
                foto_dim: {
                    'background-image': `url('Foto_shop/Piante/avocado.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: true,
                utile: false,
                altro: false,
                pianta: false,
                img: "Foto_shop/Vasi/vaso11.jpg",
                title: "Vaso color oro",
                price: "10.00",
                id: 23,
                foto_dim: {
                    'background-image': `url('Foto_shop/Vasi/vaso11.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: true,
                altro: false,
                pianta: false,
                img: "Foto_shop/Utili/irrigazione.jpg",
                title: "Set da irrigazione",
                price: "20.00",
                id: 24,
                foto_dim: {
                    'background-image': `url('Foto_shop/Utili/irrigazione.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: false,
                altro: false,
                pianta: true,
                img: "Foto_shop/Piante/camomilla.jpg",
                title: "Pianta di camomilla",
                price: "8.00",
                id: 25,
                foto_dim: {
                    'background-image': `url('Foto_shop/Piante/camomilla.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: true,
                altro: false,
                pianta: false,
                img: "Foto_shop/Utili/pala.jpg",
                title: "Pala da giardino",
                price: "12.00",
                id: 26,
                foto_dim: {
                    'background-image': `url('Foto_shop/Utili/pala.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: false,
                altro: false,
                pianta: true,
                img: "Foto_shop/Piante/ciliegia.jpg",
                title: "Pianta di ciliegie",
                price: "14.50",
                id: 27,
                foto_dim: {
                    'background-image': `url('Foto_shop/Piante/ciliegia.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: false,
                altro: false,
                pianta: true,
                img: "Foto_shop/Piante/cipolla.jpg",
                title: "Pianta di cipolle",
                price: "7.00",
                id: 28,
                foto_dim: {
                    'background-image': `url('Foto_shop/Piante/cipolla.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: true,
                altro: false,
                pianta: false,
                img: "Foto_shop/Utili/porta_attrezzi.jpg",
                title: "Kit di attrezzi completo",
                price: "28.60",
                id: 29,
                foto_dim: {
                    'background-image': `url('Foto_shop/Utili/porta_attrezzi.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: false,
                altro: false,
                pianta: true,
                img: "Foto_shop/Piante/fagioli.jpg",
                title: "Pianta di fagioli",
                price: "9.50",
                id: 30,
                foto_dim: {
                    'background-image': `url('Foto_shop/Piante/fagioli.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: true,
                utile: false,
                altro: true,
                pianta: false,
                img: "Foto_shop/Utili/terra2.jpg",
                title: "Terriccio universale di qualità",
                price: "12.85",
                id: 31,
                foto_dim: {
                    'background-image': `url('Foto_shop/Utili/terra2.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: true,
                altro: false,
                pianta: false,
                img: "Foto_shop/Vasi/vaso20.jpg",
                title: "Vaso con ringhiera",
                price: "16.50",
                id: 32,
                foto_dim: {
                    'background-image': `url('Foto_shop/Vasi/vaso20.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: false,
                altro: true,
                pianta: false,
                img: "Foto_shop/Utili/concime1.jpg",
                title: "Concime bio",
                price: "11.40",
                id: 33,
                foto_dim: {
                    'background-image': `url('Foto_shop/Utili/concime1.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: true,
                utile: false,
                altro: false,
                pianta: false,
                img: "Foto_shop/Vasi/vaso12.jpg",
                title: "Vaso in plastica",
                price: "1.50",
                id: 34,
                foto_dim: {
                    'background-image': `url('Foto_shop/Vasi/vaso12.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: false,
                altro: false,
                pianta: true,
                img: "Foto_shop/Piante/fragola.jpg",
                title: "Pianta di fragola",
                price: "4.00",
                id: 35,
                foto_dim: {
                    'background-image': `url('Foto_shop/Piante/fragola.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: true,
                altro: false,
                pianta: false,
                img: "Foto_shop/Utili/carrello.jpg",
                title: "Carrello da giardino",
                price: "12.00",
                id: 36,
                foto_dim: {
                    'background-image': `url('Foto_shop/Utili/carrello.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: false,
                altro: false,
                pianta: true,
                img: "Foto_shop/Piante/insalata.jpg",
                title: "Pianta di lattuga",
                price: "6.00",
                id: 37,
                foto_dim: {
                    'background-image': `url('Foto_shop/Piante/insalata.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: true,
                utile: false,
                altro: false,
                pianta: false,
                img: "Foto_shop/Vasi/vaso8.jpg",
                title: "Vaso Frida Kahlo",
                price: "6.50",
                id: 38,
                foto_dim: {
                    'background-image': `url('Foto_shop/Vasi/vaso8.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: true,
                altro: false,
                pianta: false,
                img: "Foto_shop/Utili/filo.jpg",
                title: "Filo di ferro per coltivazioni",
                price: "9.30",
                id: 39,
                foto_dim: {
                    'background-image': `url('Foto_shop/Utili/filo.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: false,
                altro: false,
                pianta: true,
                img: "Foto_shop/Piante/pomodoro.jpg",
                title: "Pianta di pomodori",
                price: "6.20",
                id: 40,
                foto_dim: {
                    'background-image': `url('Foto_shop/Piante/pomodoro.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: true,
                utile: false,
                altro: false,
                pianta: false,
                img: "Foto_shop/Vasi/vaso13.jpg",
                title: "Vaso rosa in plastica",
                price: "1.80",
                id: 41,
                foto_dim: {
                    'background-image': `url('Foto_shop/Vasi/vaso13.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: true,
                utile: false,
                altro: false,
                pianta: false,
                img: "Foto_shop/Vasi/vaso5.jpg",
                title: "Vasi multi-color",
                price: "12.00",
                id: 42,
                foto_dim: {
                    'background-image': `url('Foto_shop/Vasi/vaso5.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: true,
                utile: false,
                altro: false,
                pianta: false,
                img: "Foto_shop/Vasi/vaso19.jpg",
                title: "Vaso in vetro",
                price: "7.20",
                id: 43,
                foto_dim: {
                    'background-image': `url('Foto_shop/Vasi/vaso19.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: true,
                altro: false,
                pianta: false,
                img: "Foto_shop/Utili/attrezzi.jpg",
                title: "Cassetta da giardinaggio",
                price: "18.99",
                id: 44,
                foto_dim: {
                    'background-image': `url('Foto_shop/Utili/attrezzi.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: false,
                altro: true,
                pianta: false,
                img: "Foto_shop/Utili/concime2.jpg",
                title: "Concime One",
                price: "11.90",
                id: 45,
                foto_dim: {
                    'background-image': `url('Foto_shop/Utili/concime2.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: true,
                utile: false,
                altro: false,
                pianta: false,
                img: "Foto_shop/Vasi/vaso14.jpg",
                title: "Vaso quadrato verde",
                price: "6.00",
                id: 46,
                foto_dim: {
                    'background-image': `url('Foto_shop/Vasi/vaso14.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: true,
                altro: false,
                pianta: false,
                img: "Foto_shop/Utili/paletta.jpg",
                title: "Pala piccola",
                price: "6.99",
                id: 47,
                foto_dim: {
                    'background-image': `url('Foto_shop/Utili/paletta.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: true,
                utile: false,
                altro: false,
                pianta: false,
                img: "Foto_shop/Vasi/vaso15.jpg",
                title: "Vaso rosso lucido",
                price: "12.00",
                id: 48,
                foto_dim: {
                    'background-image': `url('Foto_shop/Vasi/vaso15.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: false,
                utile: true,
                altro: false,
                pianta: false,
                img: "Foto_shop/Utili/serra.jpg",
                title: "Serra da esterno",
                price: "31.70",
                id: 49,
                foto_dim: {
                    'background-image': `url('Foto_shop/Utili/serra.jpg')`
                }

            },
            {
                num: 0,
                like: 0,
                vaso: true,
                utile: false,
                altro: false,
                pianta: false,
                img: "Foto_shop/Vasi/vaso16.jpg",
                title: "Vaso celeste",
                price: "4.00",
                id: 50,
                foto_dim: {
                    'background-image': `url('Foto_shop/Vasi/vaso16.jpg')`
                }

            }

        ],
        buyitems: []
    },

    methods: {
        total: function() {
            var sum = 0;
            var res = 0;
            this.buyitems.forEach(function(buyitem) {
                res += parseFloat(buyitem.total);
                sum = res.toFixed(2); //per mettere sempre due numeri dopo la virgola
            });
            return sum;
        },
        spedizione: function() {
            var sum = 10;
            if (this.total() > 20) sum = 0;
            else if (this.total() == 0) sum = 0;
            return sum.toFixed(2);
        },

        tot: function() {
            totale = parseInt(this.total());
            if (this.spedizione() > 0)
                totale += parseInt(this.spedizione());
            return totale.toFixed(2);
        },
    },

    mounted() {
        if (localStorage.getItem('buyitems')) {
            this.buyitems = JSON.parse(localStorage.getItem('buyitems'));
        }
        if (localStorage.getItem('cuore')) {
            cuori = JSON.parse(localStorage.getItem('cuore'));
        }
        if (localStorage.getItem('conto')) {
            count = JSON.parse(localStorage.getItem('conto'));
        }
    },

    watch: {
        buyitems() {

            localStorage.setItem('buyitems', JSON.stringify(this.buyitems));
            localStorage.setItem('conto', JSON.stringify(count));
            //localStorage.setItem('conto', '0');
            //Resetta ti serve il count degli oggetti
            localStorage.setItem('cuore', JSON.stringify(cuori));
            //localStorage.setItem('cuore', '0');
            //Resetta il count dei cuori
            //localStorage.setItem('buyitems', '');
            //Resetta i buyitems
        },


    }

});

//Slider più venduti
//fa girare i più acquistati chiamando la funzione della libreria importata e passandogli i parametri

$(".categories__slider").owlCarousel({
    loop: true,
    margin: 0,
    items: 4,
    dots: false, //fa vedere i puntini per navigare
    nav: true, //fa vedere i bottoni
    navText: ["<span class='fa fa-angle-left'><span/>", "<span class='fa fa-angle-right'><span/>"],
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    smartSpeed: 1200,
    autoHeight: false,
    autoplay: true,
    responsive: { //modifica il numero di card visibili in base ai px della finestra 
        0: {
            items: 1,
        },

        480: {
            items: 2,
        },

        768: {
            items: 3,
        },

        992: {
            items: 4,
        }
    }
});


function aggiungiCuori(element) {
    if (element.classList.contains("cambiato")) {
        cuori--;
        element.classList.remove("cambiato");
    } else {
        cuori++;
        element.classList.add("cambiato");
    }
    document.getElementById("cuore").innerHTML = cuori;
    localStorage.setItem('cuore', JSON.stringify(cuori));
}