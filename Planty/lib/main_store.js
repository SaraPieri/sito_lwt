(function($) {

    $(window).on('load', function() {
        //Al clic sulle categorie (sotto i Nostri prodotti) le disattiva tutte e poi sottolinea la categoria scelta
        $('.categorie li').on('click', function() {
            $('.categorie li').removeClass('active');
            $(this).addClass('active'); //rendi attiva cioè sottolineata quella cliccata
        });
        if ($('.venduti').length > 0) {
            var containerEl = document.querySelector('.venduti');
            //seleziona tutti gli elementi nella griglia dello shop (la griglia sta nella classe featured_filter)
            var mixer = mixitup(containerEl);
            //e gli associa mixitup che è una delle lib importate per fare l'effetto mix
        }
    });
    //BACKGROUND
    //setta  nel css per ogni oggetto venduto lo sfondo dell'immagine
    $('.set-bg').each(function() {
        var bg = $(this).data('setbg'); //legge il path che è un attributo 
        $(this).css('background-image', 'url(' + bg + ')');
    });
})(jQuery);