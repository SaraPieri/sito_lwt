<?php
session_start();
?>
<!DOCTYPE html>
<html lang="ita">
<head>
    <title> Planty </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--viewport iniziale che non deve essere scalato-->
    <meta charset="UTF-8">

    <!--CSS-->
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.css">-->
    <link rel="icon" href="Foto/favicon.ico">
    <script src="https://kit.fontawesome.com/cc496d5143.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/Style.css" type="text/css">
    <link rel="stylesheet" href="css/foundations.css">
    <link rel="stylesheet" href="css/Forum.css">
 
    
    
    <link href="https://fonts.googleapis.com/css2?family=Indie+Flower&display=swap" rel="stylesheet">
    <!--<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">-->
    
    <meta http-equiv="X_UA_Compatibile" content="IE-edge">
    <!--Per Internet explorer-->

</head>

<body onload="inizializzaLogin()">
        <!--Inizio header menu-->
        <header class="header clearfix cd-main-header">
        <a href="" class="header__logo"><img src="Foto/Titolo_b.gif" width="300" height="110"></a>
        <nav class="cd-main-nav">

            <a href="" class="header__icon-bar">
                <span></span>
                <span></span>
                <span></span>
            </a>
            <!--serve se vogliamo fare la versione mobile introducendo le tre linee per aprire menu-->
            <ul class="header__menu animate cd-main-nav__list js-js-accedi-trigger">
                <li class="header__menu__item home"><a href="index.php">Home</a></li>
                <li class="header__menu__item chi"><a href="index.php#about">Chi siamo</a></li>
                <li class="header__menu__item feat"><a href="index.php#features">Come Iniziare</a></li>
                <li class="header__menu__item"><a href="Le_Tue_Piante.php">Le Tue Piante</a></li>
                <li class="header__menu__item"><a href="">Forum</a></li>
                <li class="header__menu__item"><a href="Store.html">Shop</a></li>
                    <?php if (!isset($_SESSION['nome'])){

                    ?>
                    <li class="header__menu__item">
                        <a class="cd-main-nav__item cd-main-nav__item--accedi" name="loginButton_forum" href="#0 " data-accedi="login">Accedi</a></li>
                    
                    <li class="header__menu__item">
                        <a class="cd-main-nav__item cd-main-nav__item--signup" href="#0" data-accedi="signup">Registrati</a></li>
                <?php }
                if (isset($_SESSION['nome'])){
                    echo " <li class='header__menu__item scritta'>Ciao, ".$_SESSION['nome']."</li>";
                ?>
                 <li class="header__menu__item">
                <a class="cd-main-nav__item cd-main-nav__item--accedi" href="Logout.php">Esci</a>
                </li>
                <?php 
                }?>
            </ul>
        </nav>
    </header>   

     <!--//////////LOGIN FORM///////////-->
     <div class="form js-js-accedi ">
        <!-- Form incluso il background -->
        <div class="form__container ">
            <!-- Container wrapper -->
            <ul class="form__switcher js-js-accedi-switcher js-js-accedi-trigger">
                <li><a href="#0 " data-accedi="login" data-type="login">Accedi</a></li>
                <li><a href="#0 " data-accedi="signup" data-type="signup">Registrati</a></li>
            </ul>

            <div class="form__block js-js-accedi-block" data-type="login">
                <!-- Login form -->
                <form class="form__form" action="login.php" method="post" >
                    <img src="Foto/Logo.jpg" width="550">
                    <p class="form__fieldset">
                        <label class="form__label" for="accedi-email" >E-mail</label>
                        <input class="form__input form__input--full-width form__input--has-padding form__input--has-border" id="accedi-email" type="email" placeholder="E-mail" name="inputEmail" maxlength="40" required>
                    </p>

                    <p class="form__fieldset">
                        <label class="form__label form__label--password " for="accedi-password ">Password</label>
                        <input class="form__input form__input--full-width form__input--has-padding form__input--has-border " id="accedi-password" type="password" placeholder="Password" name="inputPassword" maxlength="8" required>
                        <a href="#0 " class="form__hide-password js-hide-password">Mostra</a>
                    </p>

                    <!--<p class="form__fieldset">
                        <label for="remember-me">Ricordami</label>
                        <input type="checkbox" id="remember-me" name="remember" class="form__input" checked >

                    </p>-->

                    <p class="form__fieldset">
                        <input class="form__input form__input--full-width"  name="loginButton_forum" type="submit" value="Login">
                    </p>
                    <p class="form__bottom-message js-js-accedi-trigger"><a href="#0 " data-accedi="reset">Password dimenticata?</a></p>
                </form>


            </div>
            <!-- form__block -->

            <div class="form__block js-js-accedi-block" data-type="signup">
                <!-- Registrati form -->
                <form  action="validateregistration.php" class="form__form" method="POST" name="myForm" >
                    <p class="form__fieldset">
                        <label class="form__label form__label--username" for="signup-username ">Username</label>
                        <input type="textarea" class="form__input form__input--full-width form__input--has-padding form__input--has-border" id="signup-username" name="inputUsername" placeholder="Username" maxlength="40" required>
                    </p>

                    <p class="form__fieldset ">
                        <label class="form__label form__label--email" for="signup-email">E-mail</label>
                        <input class="form__input form__input--full-width form__input--has-padding form__input--has-border" id="signup-email" name="inputEmail" type="email" placeholder="E-mail" maxlength="40" required>
                    </p>

                    <p class="form__fieldset">
                        <label class="form__label form__label--password " for="signup-password">Password</label>
                        <input class="form__input form__input--full-width form__input--has-padding form__input--has-border" id="signup-password" name="inputPassword" type="text" placeholder="Password" maxlength="8" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{3,}" required>
                        <a href="#0 " class="form__hide-password js-hide-password" no editable>Nascondi</a>
                    </p>


                    <p class="form__fieldset">
                        <input type="submit" class="form__input form__input--full-width form__input--has-padding" name="registrationButton_forum" value="Crea Account">
                    </p>
                    <div id="message">
                        <h5>La password deve contenere:</h5>
                        <p id="letter" class="invalid">Una <b>lettera</b> minuscola</p>
                        <p id="capital" class="invalid">Una <b>lettera</b> maiuscola</p>
                        <p id="number" class="invalid">Un <b>numero</b></p>
                    </div>
                </form>
            </div>
            <!-- form__block -->

            <div class="form__block js-js-accedi-block" data-type="reset">
                <!-- Reset password form -->
                <p class="form__message">Hai dimenticato la password? </p>
                <p class="form__message">Inserisci la tua mail e riceverai il link per generarne una nuova.</p>

                <form class="form__form">
                    <p class="form__fieldset">
                        <label class="form__label form__label--email" for="reset-email">E-mail</label>
                        <input class="form__input form__input--full-width form__input--has-padding form__input--has-border" id="reset-email" type="email" placeholder="E-mail" maxlength="40">
                    </p>

                    <p class="form__fieldset">
                        <input class="form_t_input form__input--full-width form__input--has-padding" type="submit" value="Reset password">
                    </p>

                    <p class="form__bottom-message js-js-accedi-trigger"><a href="#0" data-accedi="login">Torna al log-in</a></p>
                </form>


            </div>
            <!-- form__block -->

        </div>
        <!-- form__container -->
    </div>
    <!--/////////////////FINE LOGIN//////////////-->
    

    <div class="row">
        <!--<a href="#" class="underline">Hai in mente qualcosa? Crea un topic!</a>-->
        <div class="lpad divisione"></div>
    </div> 


                <?php if (!isset($_SESSION['nome'])){
                echo "<p class='padding_p'>Devi effettuare il Login per accedere al Forum</p>";
                    }
                else {
                        $dbconn=pg_connect("host=localhost port=5432 dbname=postgres user=postgres password=ulivo") or die ('Could connect: ' . pg_last_error());
                        $nome=$_SESSION['nome'];
                        $q1="SELECT * FROM topic";
                        $result=pg_query($q1);
                        if(pg_num_rows($result)==0)  {echo "<p class='padding_p'> Non ci sono topic pubblicati</p>";}
                        while($line=pg_fetch_array($result, null, PGSQL_ASSOC)){
                            $creatore=$line['creatore'];
                            $topic=$line['topic'];
                            $testo=$line['testo'];
                            $ora=$line['ora'];
                            $data=$line['data']; 
                            $q2="SELECT * FROM risposte r where r.topic='".$topic."'";
                            $result2=pg_query($q2);
                            $numero_post=pg_num_rows($result2);
                            $q3="SELECT distinct r.autore
                            from risposte r 
                            where r.topic='".$topic."' and r.autore!='".$creatore."'"; 
                            $result3=pg_query($q3);
                            $risposte=pg_num_rows($result3);
                            ?>

    <div class="row mt">
        <div class="large-12">
            <div class="large-12 forum-category rounded top">
                <div class="large-8 small-10 column lpad">
                 Topic <?php echo $topic;?>
                </div>
                <div class="large-4 small-2 column lpad ar">
                    <a data-connect>
                    
                        <i class="fas fa-arrow-up"></i>
                    </a>
                </div>
            </div>
        
            <div class="toggleview">

                <div class="large-12 forum-head">
                    <div class="large-8 small-8 column lpad">
                        Forum
                    </div>
                    <div class="large-1 column lpad">
                        Post
                    </div>
                    <div class="large-1 column lpad">
                        Partecipano
                    </div>
                    <div class="large-2 small-4 column lpad">
                        Last
                    </div>
                </div>
                
                <div class="large-12 forum-topic">
                    <div class="large-1 column lpad">
                        <i class="fas fa-seedling"></i>

                    </div>
                    <div class="large-7 small-8 column lpad">
                        <span class="elem-forum">
                        <a href="#"><?php echo $topic?></a>
                        </span>
                        <span class="elem-forum">
                        <?php echo $testo?>
                        </span>
                    </div>
                    <div class="large-1 column lpad">
                        <span class="center"><?php echo $numero_post+1?></span>
                    </div>
                    <div class="large-1 column lpad">
                        <span class="center"><?php echo $risposte?></span>
                    </div>
                    <div class="large-2 small-4 column pad">
                        <span>
                        <a href="#"><?php echo $topic?></a>
                        </span>
                        <span><?php echo $data." ".$ora?></span>
                        <span>by <a href="#"><?php echo $creatore?></a></span>
                    </div>
                </div>

                <?php 
                            while($risposta=pg_fetch_array($result2, null, PGSQL_ASSOC)){
                                $autore_risp=$risposta['autore'];
                                $testo_risp=$risposta['testo'];
                                $ora_risp=$risposta['ora'];
                                $data_risp=$risposta['data']; ?>

                <div class="large-12 forum-topic">
                    <div class="large-1 column lpad">
                        
                        <i class="far fa-comments"></i>
                    
                    </div>
                    <div class="large-7 small-8 column lpad">
                        <span class="elem-forum">
                        <a href="#"><?php echo 'Risposta: '.$topic?></a>
                        </span>
                        <span class="elem-forum">
                        <?php echo $testo_risp?>
                        </span>
                    </div>
                   
                    <div class="large-1 column lpad">
                        <span class="center"><?php echo $numero_post+1?></span>
                    </div>
                    <div class="large-1 column lpad">
                        <span class="center"><?php echo $risposte?></span>
                    </div>
                    <div class="large-2 small-4 column pad">
                        <span>
                        <a href="#"><?php echo $topic?></a>
                        </span>
                        <span><?php echo $data_risp." ".$ora_risp?></span>
                        <span>by <a href="#"><?php echo $autore_risp?></a></span>
                    </div>
                </div>

                <?php }?>

                <div class="large-12 forum-topic">
                    <div class="large-1 column lpad">
                        <i class="far fa-comment"></i>
                    </div>
                    <div class="large-7 column lpad">
                        <span class="elem-forum">
                            <a href="#">Partecipa al topic</a>
                        </span>
                        <span class="elem-forum">
                            <form  action="Crea_commento.php?topic=<?php echo $topic?>" method="POST" name="crea_commento" >
                                <input type="text" id="testo" name="testo_commento" placeholder="Scrivi qui!" maxlength="250" required>
                                <input type="submit" class="btn_send" name="save_commento">
                            </form>
                        </span>
                    </div>
                    <div class="large-1 column lpad">
                        <span class="center"></span>
                    </div>
                    <div class="large-1 column lpad">
                        <span class="center"></span>
                    </div>
                    <div class="large-2 small-4 column pad">
                        <span>
                        <i class="far fa-paper-plane"></i>
                        </span>
                       
                        <span>by <a href="#"><?php echo $nome?></a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

                <?php } ?>
            
                <?php }?>
                
    <?php if (isset($_SESSION['nome'])){?>
    <div class="row mt">
        <div class="large-12">
            <div class="large-12 forum-category rounded top">
                <div class="large-8 small-10 column lpad">
                Nuovo
                </div>
                <div class="large-4 small-2 column lpad ar">
                <a data-connect>
                    <i class="fas fa-arrow-up"></i>
                </a>
                </div>
            </div>
        
            <div class="toggleview">

                <div class="large-12 forum-head">
                    <div class="large-8 small-8 column lpad">
                        Forum
                    </div>
                    <div class="large-1 column lpad">
                        Post
                    </div>
                    <div class="large-1 column lpad">
                        Partecipano
                    </div>
                    <div class="large-2 small-4 column lpad">
                        Last
                    </div>
                </div>
                <div class="large-12 forum-topic">
                    <div class="large-1 column lpad">
                    <i class="fas fa-pencil-alt"></i>
                    </div>
                    <div class="large-7 small-8 column lpad">
                        <span class="elem-forum">
                        <a href="#">Crea topic</a>
                        </span>
                        <span class="elem-forum">
                            <form  action="Crea_topic.php" method="POST" name="crea_topic" style="display:inline;">
                                <input type="text" name="title_topic" placeholder="Il tuo titolo" maxlength="50" required>
                                <input type="textarea" rows="4" cols="50" id="testo" name="text_topic" placeholder="Scrivi qui!" maxlength="250" required>
                                <input type="submit" class="btn_send" name="save_topic">
                            </form>
                        </span>
                    </div>
                    <div class="large-1 column lpad">
                        <span class="center">0</span>
                    </div>
                    <div class="large-1 column lpad">
                        <span class="center">0</span>
                    </div>
                    <div class="large-2 small-4 column pad">
                        <span>
                        <i class="far fa-paper-plane"></i>
                        </span>
                       
                        <span>by <a href="#"><?php echo $nome?></a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

                <?php } ?>
    

        
        
    <div class="row mt mb">
            <div class="large-12">
            <div class="large-12 small-12 forum-category rounded top lpad">
                <span>Info</span>
            </div>
            <!--Normal per il background e lo styling-->
                <div class="large-12 small-12 normal lpad">
                    <h1 class="inset">Regole del forum </h1>
                    <p>Ciao, siamo contenti di vederti! Vuoi sapere perche la tua pianta non cresce o capire come installare una serra spaziale?! Partecipa al forum e aiutaci a rendere viva la discussione. Mi raccomando, anche se sei in disaccordo su qualcosa, rispetta gli argomenti di discussione e le persone che ne stanno parlando. Inoltre ti invitiamo a partire dalle discussioni esistenti quando vuoi postare qualcosa, leggere gli interventi degli altri prima di scrivere il proprio, scoprire gli argomenti di discussione in corso prima di aprirne uno nuovo. In questo modo avrai migliori opportunità di entrare in contatto con gli altri che condividono i tuoi stessi interessi.</p>

                </div>
            </div>
    </div>


<a class="scrolltop padd_freccia" href="#" id="myTop"><span class="fa fa-angle-up my_size"></span></a>
<div class="padding_footer"></div>

       
        <!--Footer-->
        <footer class="footer">
            <div class="row_footer">
                <div class="col-lg-6 col-xs-12 text-lg-left text-center">
                    <p class="end-text">
                        “To plant a garden is to believe in tomorrow.” H.A.
                    </p>
                </div>
                <div class="col-lg-6 col-xs-12 text-lg-right text-center">
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <a href="index.php">Home</a>
                        </li>

                        <li class="list-inline-item chi">
                            <a href="index.php#about">Chi Siamo</a>
                        </li>

                        <li class="list-inline-item feat">
                            <a href="index.php#features">Come Iniziare</a>
                        </li>

                        <li class="list-inline-item">
                            <a href="Le_Tue_Piante.php">Le Tue Piante</a>
                        </li>

                        <li class="list-inline-item">
                            <a href="">Forum</a>
                        </li>

                        <li class="list-inline-item">
                            <a href="Store.html">Shop</a>
                        </li>
                    </ul>
                </div>
            </div>
        </footer>
    <!--Fine footer-->



</body>
<script src="lib/jquery/jquery.min.js"></script>
<script type= "text/javascript" lang="javascript" src="lib/main.js "></script>
<script type= "text/javascript" lang="javascript" src="lib/Forum.js"></script>
<script type= "text/javascript" lang="javascript" src="lib/counterup/counterup.min.js"></script>
<script type= "text/javascript" lang="javascript" src="lib/custom.js"></script>

</html>