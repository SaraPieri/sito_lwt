<!DOCTYPE html>
<html>
<?php
session_start();
?>
    <html>

    <head>
        <title> Planty</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">

        <!--CSS-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.css">
        <!--lo inseriamo per avere delle prestazioni migliori-->
        <link rel="stylesheet" href="css/Style.css" type="text/css">

        <!--Non penso serva il seguente css-->
        <!--<link rel="stylesheet" href="css/anim.css" type="text/css">-->

        <link rel="icon" href="Foto/favicon.ico">
        <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!--serve per la scritta del "Ciao,nome_utente"-->
        <link href="https://fonts.googleapis.com/css2?family=Indie+Flower&display=swap" rel="stylesheet">


        <meta http-equiv="X_UA_Compatibile" content="IE-edge">
        <!--Per Internet explorer-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--viewport iniziale che non deve essere scalato-->
    </head>

    <body>
        <style>
            body {
                background-color: #fdb580;
            }
            
            html {
                /*per evitare che scali in orizzontale (soluzione bruta)*/
                overflow-x: hidden;
            }
        </style>
        <!--Fine schermata iniziale-->
        <!--Inizio header menu-->
        <header class="header clearfix cd-main-header" width=100%>
            <a href="" class="header__logo"><img src="Foto/Titolo_b.gif" width="300" height="110"></a>
            <nav class="cd-main-nav">

                <a href="" class="header__icon-bar">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
                <!--serve se vogliamo fare la versione mobile introducendo le tre linee per aprire menu-->
                <ul class="header__menu animate cd-main-nav__list ">
                    <li class="header__menu__item"><a href="index.php">Home</a></li>
                    <li class="header__menu__item"><a href="index.php#about">Chi siamo</a></li>
                    <li class="header__menu__item"><a href="index.php#features">Come Iniziare</a></li>
                    <li class="header__menu__item"><a href="Le_Tue_Piante.php">Le Tue Piante</a></li>
                    <li class="header__menu__item"><a href="Forum.php">Forum</a></li>
                    <li class="header__menu__item"><a href="Store.html">Shop</a></li>
                    <?php 
                if (isset($_SESSION['nome'])){
                    echo "<li class='header__menu__item scritta'>Ciao, ".$_SESSION['nome']."</li>";
                ?>
                <li class="header__menu__item">
                <a class="cd-main-nav__item cd-main-nav__item--accedi" href="Logout.php">Esci</a>
                </li>  
                <?php 
                }
                ?>
                   
                </ul>
            </nav>
        </header>
    </section>
        <div class="container">
            <img class="center flex" src="Foto/Piante_header_Yeah.jpg">
        </div>
       
        <div class="row">

            <div class="col-lg-4 orologio" max-width="300 px">
                <div id="picture">
                    <div id="picture-1"></div>
                    <div id="picture-2"></div>
                    <div id="picture-3"></div>
                </div>
            </div>
            
            <div class="col-lg-4 orologio ">
                <section class="border-clock"></section>
                <section class="clock">
                    <output class="date"></output>
                    <div class="minutes"></div>
                    <div class="hours"></div>
                    <div class="seconds"></div>
                    <div class="cercle"></div>
                </section>
            </div>

           <!--POST-ITS-->
            <div class="col-lg-4 orologio">

                <div class="post-it">
                    <p class="sticky taped">
                        <strong>Foto di Gruppo!</strong><br> Clicca sulla cornice <br> per scegliere <br> cosa piantare
                    </p>
                </div>
            </div>

        </div>
        
        <section class="block no-padding block-pd-lg block-bg-overlay text-center">


            <img id="foto center flex" src="Foto/Gruppo.png" usemap="#image-map">
            <map name="image-map">
            <area class="ananas" target="" alt="ananas" title="ananas" href="" coords="48,338,209,659" shape="rect">
            <area class="avocado" target="" alt="avocado" title="avocado" href="" coords="219,470,325,627" shape="rect">
            <area class="cipolla" target="" alt="cipolla" title="cipolla" href="" coords="334,411,446,627" shape="rect">
            <area class="carota" target="" alt="carota" title="carota" href="" coords="448,440,502,644" shape="rect">
            <area class="pomodoro" target="" alt="pomodoro" title="pomodoro" href="" coords="513,482,603,607" shape="rect">
            <area class="patata" target="" alt="patata" title="patata" href="" coords="613,494,705,612" shape="rect">
            <area class="banana" target="" alt="banana" title="banana" href="" coords="707,458,810,643" shape="rect">
            <area class="lattuga" target="" alt="lattuga" title="lattuga" href="" coords="815,488,964,644" shape="rect">
            </map>

        </section>

        <span id="zonaDinamica" class="zona" width=200px></span>
        
        <script>
            var documenti = document.getElementsByTagName("area");
            for (var i = 0; i < documenti.length; i++) {
                documenti[i].onclick = caricaPagina;
            }

            function caricaPagina(e) {
                e.preventDefault();
                var httpRequest = new XMLHttpRequest();
                httpRequest.prevTarget = e.target;
                httpRequest.onreadystatechange = gestisciResponse;
                httpRequest.open("GET", "Dinamiche/" + e.target.getAttribute("class") + ".html", true);
                httpRequest.send();
            }

            function gestisciResponse(e) {
                if (e.target.readyState == 4 && e.target.status == 200) {
                    document.getElementById("zonaDinamica").innerHTML =
                        e.target.responseText;
                }
            }
        </script>


        <!--Footer-->
        <footer class="footer">
            <div class="row">
                <div class="col-lg-6 col-xs-12 text-lg-left text-center">
                    <p class="end-text">
                        “To plant a garden is to believe in tomorrow.” H.A.
                    </p>
                </div>
                <div class="col-lg-6 col-xs-12 text-lg-right text-center">
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <a href="index.php">Home</a>
                        </li>

                        <li class="list-inline-item">
                            <a href="index.php#about">Chi Siamo</a>
                        </li>

                        <li class="list-inline-item">
                            <a href="index.php#features">Come Iniziare</a>
                        </li>

                        <li class="list-inline-item">
                            <a href="Le_Tue_Piante.php">Le Tue Piante</a>
                        </li>

                        <li class="list-inline-item">
                            <a href="Forum.php">Forum</a>
                        </li>

                        <li class="list-inline-item">
                            <a href="Store.html">Shop</a>
                        </li>
                    </ul>
                </div>
            </div>
        </footer>
        <!--Fine footer-->
        <a class="scrolltop" href="#" id="myTop"><span class="fa fa-angle-up"></span></a>

    </body>
    <script src="lib/main.js"></script>
    <script src="lib/jquery/jquery.min.js "></script>
    <script type="text/javascript" lang="javascript" src="lib/counterup/counterup.min.js"></script>
    <script type="text/javascript" lang="javascript" src="lib/waypoints/waypoints.min.js"></script>
    <script type="text/javascript" lang="javascript" src="lib/jquery/jquery-migrate.min.js"></script>
    <script src="lib/custom.js"></script>
    <script src="lib/orologio.js"></script>
    <script src="lib/stickyjs/sticky.js"></script>

</html>