<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <title> Planty</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">

    <!--CSS-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.css">
    <!--lo inseriamo per avere delle prestazioni migliori-->
    <link rel="stylesheet" href="css/Style.css" type="text/css">
    <link rel="icon" href="Foto/favicon.ico">
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!--serve per la scritta del "Ciao,nome_utente"-->
    <link href="https://fonts.googleapis.com/css2?family=Indie+Flower&display=swap" rel="stylesheet">
    
    
    <meta http-equiv="X_UA_Compatibile" content="IE-edge">
    <!--Per Internet explorer-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--viewport iniziale che non deve essere scalato-->
</head>

<body onload="inizializzaLogin()">
    <!--onload definisce un evento che parte appena si avvia la pagina-->
    <!-- Schermata iniziale-->  

    <section class="hero">
        <div class="container text-center">
            <!--le classi row, col-md-12, tagline sono di bootstrap per gestire lo spazio del container-->
            <div class="row">
                <!--class= row é quella che contiene il logo sulla finestra-->
                <div class="col-md-12">
                    <a class="hero-brand" href="index.php" title="Home">
                        <img alt="Planty Logo" width="350" height="140" src="Foto/Titolo_b.gif"></a>
                </div>
            </div>

            <div class="col-md-12"> <!--usano la stessa colonna-->
                <p class="tagline">
                    Semi gratuiti in frigo o in dispensa, per trasformare la tua casa in un giardino!
                </p>
                <!--Ha il riferimento ad about per scendere nella schemata se cliccato ma non l'ho ancora fatto funzionare
                infatti la classe che contiene il chi siamo ha classe about-->
                <a class="btnn" href="#about">Scopri di più</a>
            </div>
        </div>
    </section>


    <!--Fine schermata iniziale-->
    <!--Inizio header menu-->
    <header class="header clearfix cd-main-header">
        <a href="" class="header__logo"><img src="Foto/Titolo_b.gif" width="300" height="110"></a>
        <nav class="cd-main-nav">

            <a href="" class="header__icon-bar">
                <span></span>
                <span></span>
                <span></span>
            </a>
            <!--serve se vogliamo fare la versione mobile introducendo le tre linee per aprire menu-->
            <ul class="header__menu animate cd-main-nav__list js-js-accedi-trigger">
                <li class="header__menu__item home"><a href="index.php">Home</a></li>
                <li class="header__menu__item chi"><a href="#about">Chi siamo</a></li>
                <li class="header__menu__item feat"><a href="#features">Come Iniziare</a></li>
                <li class="header__menu__item"><a href="Le_Tue_Piante.php">Le Tue Piante</a></li>
                <li class="header__menu__item"><a href="Forum.php">Forum</a></li>
                <li class="header__menu__item"><a href="Store.html">Shop</a></li>
                    <?php if (!isset($_SESSION['nome'])){

                    ?>
                    <li class="header__menu__item">
                        <a class="cd-main-nav__item cd-main-nav__item--accedi" name="loginButton" href="#0 " data-accedi="login">Accedi</a></li>
                    
                    <li class="header__menu__item">
                        <a class="cd-main-nav__item cd-main-nav__item--signup" name="registrationButton_index" href="#0" data-accedi="signup">Registrati</a></li>
                <?php }
                if (isset($_SESSION['nome'])){
                    echo " <li class='header__menu__item scritta'>Ciao, ".$_SESSION['nome']."</li>";
                ?>
                 <li class="header__menu__item">
                <a class="cd-main-nav__item cd-main-nav__item--accedi" href="Logout.php">Esci</a>
                </li>
                <?php 
                }?>
            </ul>
        </nav>
    </header>

    <!--//////////LOGIN FORM///////////-->
    <div class="form js-js-accedi ">
        <!-- Form incluso il background -->
        <div class="form__container ">
            <!-- Container wrapper -->
            <ul class="form__switcher js-js-accedi-switcher js-js-accedi-trigger">
                <li><a href="#0 " data-accedi="login" data-type="login">Accedi</a></li>
                <li><a href="#0 " data-accedi="signup" data-type="signup">Registrati</a></li>
            </ul>

            <div class="form__block js-js-accedi-block" data-type="login">
                <!-- Login form -->
                <form class="form__form" action="login.php" method="post" >
                    <img src="Foto/Logo.jpg" width="550">
                    <p class="form__fieldset">
                        <label class="form__label" for="accedi-email" >E-mail</label>
                        <input class="form__input form__input--full-width form__input--has-padding form__input--has-border" id="accedi-email" type="email" placeholder="E-mail" name="inputEmail" maxlength="40" required>
                    </p>

                    <p class="form__fieldset ">
                        <label class="form__label form__label--password " for="accedi-password ">Password</label>
                        <input class="form__input form__input--full-width form__input--has-padding form__input--has-border" id="accedi-password" type="password" placeholder="Password" name="inputPassword" maxlength="8" required>
                        <a href="#0 " class="form__hide-password js-hide-password">Mostra</a>
                    </p>

                    <p class="form__fieldset">
                        <input class="form__input form__input--full-width" name="loginButton" type="submit" value="Login">
                    </p>
                    <p class="form__bottom-message js-js-accedi-trigger"><a href="#0 " data-accedi="reset">Password dimenticata?</a></p>
                </form>


            </div>
            <!-- form__block -->

            <div class="form__block js-js-accedi-block" data-type="signup">
                <!-- Registrati form -->
                <form  action="validateregistration.php" class="form__form" method="POST" name="myForm" >
                    <p class="form__fieldset">
                        <label class="form__label form__label--username" for="signup-username ">Username</label>
                        <input type="textarea" class="form__input form__input--full-width form__input--has-padding form__input--has-border" id="signup-username" name="inputUsername" placeholder="Username" maxlength="40" required>
                    </p>

                    <p class="form__fieldset ">
                        <label class="form__label form__label--email" for="signup-email">E-mail</label>
                        <input class="form__input form__input--full-width form__input--has-padding form__input--has-border" id="signup-email" name="inputEmail" type="email" placeholder="E-mail" maxlength="40" required>
                    </p>

                    <p class="form__fieldset">
                        <label class="form__label form__label--password " for="signup-password">Password</label>
                        <input class="form__input form__input--full-width form__input--has-padding form__input--has-border" id="signup-password" name="inputPassword" type="text" placeholder="Password" maxlength="8" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{3,}" required>
                        <a href="#0 " class="form__hide-password js-hide-password" no editable>Nascondi</a>
                    </p>

                   
                    <p class="form__fieldset">
                        <input type="submit" class="form__input form__input--full-width form__input--has-padding" name="registrationButton" value="Crea Account">
                    </p>
                    <div id="message">
                        <h5>La password deve contenere:</h5>
                        <p id="letter" class="invalid">Una <b>lettera</b> minuscola</p>
                        <p id="capital" class="invalid">Una <b>lettera</b> maiuscola</p>
                        <p id="number" class="invalid">Un <b>numero</b></p>
                    </div>
                </form>
            </div>
            <!-- form__block -->

            <div class="form__block js-js-accedi-block" data-type="reset">
                <!-- Reset password form -->
                <p class="form__message">Hai dimenticato la password? </p>
                <p class="form__message">Inserisci la tua mail e riceverai il link per generarne una nuova.</p>

                <form class="form__form">
                    <p class="form__fieldset">
                        <label class="form__label form__label--email" for="reset-email">E-mail</label>
                        <input class="form__input form__input--full-width form__input--has-padding form__input--has-border" id="reset-email" type="email" placeholder="E-mail" maxlength="40">
                    </p>

                    <p class="form__fieldset">
                        <input class="form__input form__input--full-width form__input--has-padding" type="submit" value="Reset password">
                    </p>

                    <p class="form__bottom-message js-js-accedi-trigger"><a href="#0" data-accedi="login">Torna al log-in</a></p>
                </form>
            </div>
            <!-- form__block -->

        </div>
        <!-- form__container -->
    </div>
    <!--/////////////////FINE LOGIN//////////////-->


    <!--Chi siamo con cerchi-->
    <section class="about" id="about">
        <div class="container text-center">
            <h2 class="color_blue">
                Chi siamo
            </h2>

            <h5 class="color_blue">
                2020 Quarantena.
            </h5>

            <p>"...durante questi giorni piantano tutti qualcosa, io non ho neanche un seme a casa ".</p>
            <p>"Non ci credo...è impossibile, scommettiamo che non sai di averli? "</p>
            
            <!--<img src="Foto/quaderno2.jpg" width="300" > Foto del progetto-->
            <!--Cerchi usa le classi di bootstrap row/circle/stats-col, il data-toggle non è implementato per il conto realistico-->
            <div class="row stats-row " onload="Counter()">
                <div class="stats-col text-center col-md-3 col-sm-6 ">
                    <div class="circle ">
                        <span class="stats-no " data-toggle="counter-up">8</span> Idee da piantare subito
                    </div>
                </div>

                <div class="stats-col text-center col-md-3 col-sm-6 ">
                    <div class="circle ">
                        <span class="stats-no " data-toggle="counter-up">0</span>Meno scarti e zero soldi spesi.
                    </div>
                </div>

                <!--Query al server per il numero di utenti-->
                <?php
                    $dbconn=pg_connect("host=localhost port=5432 dbname=postgres user=postgres password=ulivo") or die ('Could connect: ' . pg_last_error());
                    $q1="SELECT * FROM organizzazione"; 
                    $result=pg_query($q1);    
                ?>
                <div class="stats-col text-center col-md-3 col-sm-6 ">
                    <div class="circle ">
                        <span class="stats-no " data-toggle="counter-up">
                        <?php
                          if($result){
                            $contadini=pg_num_rows($result);
                            echo $contadini; 
                            }
                          else {
                              echo "0";
                          }
                        ?></span>Nuovi Contadini
                    </div>
                </div>

                <div class="stats-col text-center col-md-3 col-sm-6 ">
                    <div class="circle">
                        <span class="stats-no " data-toggle="counter-up"><?php
                          $q2="SELECT * FROM piante";
                          $result2=pg_query($q2);
                          if($result2){
                            $co2=pg_num_rows($result2);
                            echo $co2*20; 
                            }
                          else {
                              echo "0";
                          }
                        
                        ?></span> Chili di CO2 assorbiti
                    </div>
                </div>
            </div>
            <!--Fine cerchi-->
        </div>
    </section>
    <!--Fine chi siamo-->

    <section>
        <div class="block block-pd-lg block-bg-overlay text-center ">
            <h2>
                DAI TUOI SCARTI
            </h2>

            <p>
                Piante da comprare una volta ed usare per sempre
            </p>
            <img class="center" src="Foto/FrigoAperto.jpg ">
        </div>
    </section>
    <!-- Features -->

    <section class="features" id="features">
        <div class="container">
            <h2 class="text-center">
                Come iniziare?
            </h2>

            <div class="row">
                <div class="feature-col col-lg-4 col-xs-12">
                    <div class="cella  text-center">
                        <div>
                            <div class="feature-icon">
                                <span class="fa fa-sign-in"></span> 
                            </div>
                        </div>

                        <div>
                            <h3>
                                Iscriviti
                            </h3>

                            <p>
                                Entra con noi!
                            </p>
                        </div>
                    </div>
                </div>

                <div class="feature-col col-lg-4 col-xs-12">
                    <div class="cella  text-center">
                        <div>
                            <div class="feature-icon">
                                <span class="fa fa-leaf"></span>
                            </div>
                        </div>

                        <div>
                            <h3>
                                Aggiungi una nuova pianta
                            </h3>

                            <p>
                                Hai una banana in cucina un po' troppo matura o vuoi svelare i segreti del pomodoro? Scegli la tua prima piantina da far nascere.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="feature-col col-lg-4 col-xs-12">
                    <div class="cella  text-center">
                        <div>
                            <div class="feature-icon">
                                <span class="fa fa-pencil"></span>
                            </div>
                        </div>

                        <div>
                            <h3>
                                Personalizzala
                            </h3>

                            <p>
                                Scegli il nome, la storia e la data di nascita della tua piantina. Personalizzandola sarà davvero speciale.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="feature-col col-lg-4 col-xs-12">
                    <div class="cella  text-center">
                        <div>
                            <div class="feature-icon">
                                <span class="fa fa-heart"></span>
                            </div>
                        </div>

                        <div>
                            <h3>
                                Prenditene cura
                            </h3>

                            <p>
                                Cura la tua pianta e ottieni grandi soddisfazioni. Costruisci da quello che hai in casa una zona verde tutta tua.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="feature-col col-lg-4 col-xs-12">
                    <div class="cella  text-center">
                        <div>
                            <div class="feature-icon">
                                <span class="fa fa-comments"></span>
                            </div>
                        </div>

                        <div>
                            <h3>
                                Forum
                            </h3>

                            <p>
                                Condividi i tuoi progressi con la community, e ottieni nuovi consigli.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="feature-col col-lg-4 col-xs-12">
                    <div class="cella  text-center">
                        <div>
                            <div class="feature-icon">
                                <span class="fa fa-shopping-cart"></span>
                            </div>
                        </div>

                        <div>
                            <h3>
                                Shop
                            </h3>

                            <p>
                                Compra gli accessori e le piante di cui non puoi proprio fare a meno.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Features -->

    <section>
        <!--Inizio Banner-->
        <div class="banner clearfix ">
            <!--<div class="banner__image ">img</div>-->

            <div class="conta">
                <h2>Ti manca qualcosa?</h2>
                <p>Hai finito la terra, il concime? Vuoi comprare un vaso o una pianta che non hai?</p>
                <a class="btnn btnn-ghost " href="Store.html">Vai allo Store</a>

            </div>

        </div>
    </section>

    <!--Sezione immagini l'ho fatta simile a quella sul tutorial che mi hai mandato ma il testo secondo 
        me è meglio farlo apprire quando ci passi sopra con il mouse-->
    <section class="clearfix text-center">
        <container class="text color_blue">
            <h2>
                Le Nostre Proposte
            </h2>
            <p>
                Trovali in casa tua, noi ti spiegheremo come piantarli!
            </p>
            <div class="card ">
                <img class="card_image " src="Foto/Personaggi/Carota.jpg " alt="Carota ">
                <span class="caption fade-caption">
                    <h2>La Carota</h2>
                    <p class="card_text">Salutare e benefica. La carota fa veramente bene a tutto! </p>
                </span>
            </div>
            <div class="card ">
                <img class="card_image " src="Foto/Personaggi/Ananas.jpg " alt="Ananas ">
                <span class="caption fade-caption">
                    <h2>L'Ananas</h2>
                    <p class="card_text" >Gioia e vivacità. E' la pianta simbolo del divertimento e dell'allegria.</p>
                </span>
            </div>
            <div class="card ">
                <img class="card_image " src="Foto/Personaggi/Pomodoro.jpg " alt="Pomodoro ">
                <span class="caption fade-caption">
                    <h2>Il Pomodoro</h2>
                    <p class="card_text">Immancabile e generoso. Per chi non manca di ottimismo e buonumore.</p>
                </span>
            </div>
            <div class="card ">
                <img class="card_image " src="Foto/Personaggi/Lattuga.jpg " alt="Lattuga ">
                <span class="caption fade-caption">
                    <h2>La Lattuga</h2>
                    <p class="card_text">Freschezza e autenticità. Semplice e fondamentale.</p>
                </span>
            </div>

            <div class="card ">
                <img class="card_image " src="Foto/Personaggi/Avocado.jpg " alt="Avocado ">
                <span class="caption fade-caption">
                    <h2>L'Avocado</h2>
                    <p class="card_text" >Per i romantici e gli affettuosi. I frutti di avocado crescono sempre in coppia. </p>
                </span>
            </div>
            <div class="card ">
                <img class="card_image " src="Foto/Personaggi/Cipolla.jpg " alt="Cipolla ">
                <span class="caption fade-caption">
                    <h2>La Cipolla</h2>
                    <p class="card_text" >Forte ma dal cuore tenero, ha un sapore forte ma chi non ha mai pianto davanti ad una cipolla? </p>
                </span>
            </div>
            <div class="card ">
                <img class="card_image " src="Foto/Personaggi/Banana.jpg " alt="Banana ">
                <span class="caption fade-caption">
                    <h2 class="card_text particular">La Banana</h2>
                    <p class="card_text particular">Determinazione e delicatezza. Il banano è bello e delicato, per chi sa aspettare per cogliere il meglio dalle cose! </p>
                </span>
            </div>
            <div class="card ">
                <img class="card_image " src="Foto/Personaggi/Patata.jpg " alt="Patata ">
                <span class="caption fade-caption">
                    <h2 class="card_text particular">La Patata</h2>
                    <p class="card_text particular" >Intuizione e fantasia. Sbrizzarrisciti, è un alimento contenuto in tantissime ricette, non solo...può essere usata per generare elettricità. </p>
                </span>
            </div>
        </container>
    </section>


    <!--Fine delle cards-->

    <!--Footer-->
    <footer class="footer">
        <div class="row">
            <div class="col-lg-6 col-xs-12 text-lg-left text-center">
                <p class="end-text">
                    “To plant a garden is to believe in tomorrow.” H.A.
                </p>
            </div>
            <div class="col-lg-6 col-xs-12 text-lg-right text-center">
                <ul class="list-inline">
                    <li class="list-inline-item">
                        <a href="index.php">Home</a>
                    </li>

                    <li class="list-inline-item chi">
                        <a href="#about">Chi Siamo</a>
                    </li>

                    <li class="list-inline-item feat">
                        <a href="#features">Come Iniziare</a>
                    </li>

                    <li class="list-inline-item">
                        <a href="Le_Tue_Piante.php">Le Tue Piante</a>
                    </li>

                    <li class="list-inline-item">
                        <a href="Forum.php">Forum</a>
                    </li>

                    <li class="list-inline-item">
                        <a href="Store.html">Shop</a>
                    </li>
                </ul>
            </div>
        </div>
    </footer>
    <!--Fine footer-->

    <a class="scrolltop" href="#" id="myTop"><span class="fa fa-angle-up"></span></a>
    <script>

</script>
    <!-- Resource JavaScript -->
    <script type= "text/javascript" lang="javascript" src="lib/main.js "></script>
    <script type= "text/javascript" lang="javascript" src="lib/jquery/jquery.min.js "></script>
    <!--Librerie necessarie per il counter-->
    <script type= "text/javascript" lang="javascript" src="lib/counterup/counterup.min.js"></script>
    <script type= "text/javascript" lang="javascript" src="lib/waypoints/waypoints.min.js"></script>
    <script type= "text/javascript" lang="javascript" src="lib/jquery/jquery-migrate.min.js"></script>
    <!--Qui ci sono gli script JQuery e la funzione onready-->
    <script type= "text/javascript" lang="javascript" src="lib/custom.js"></script>
    <!--Importa la libreria per bloccare il menu e usa una funzione in custom.js che setta lo spazio in alto =0-->
    <script type= "text/javascript" lang="javascript" src="lib/stickyjs/sticky.js"></script>
</body>


</html>