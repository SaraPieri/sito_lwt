<?php
session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <title> Planty</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">

    <!--CSS-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.css">
    <!--lo inseriamo per avere delle prestazioni migliori-->
    <link rel="stylesheet" href="css/Style.css" type="text/css">
    <link rel="icon" href="Foto/favicon.ico">
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/Le_tue_piante.css" type="text/css">
     <!--serve per la scritta del "Ciao,nome_utente"-->
     <link href="https://fonts.googleapis.com/css2?family=Indie+Flower&display=swap" rel="stylesheet">


    <meta http-equiv="X_UA_Compatibile" content="IE-edge">
    <!--Per Internet explorer-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--viewport iniziale che non deve essere scalato-->
</head>

<body onload="inizializzaLogin()">
    <!--onload definisce un evento che parte appena si avvia la pagina-->
    <!--Inizio header menu-->
    <header class="header clearfix cd-main-header">
        <a href="" class="header__logo"><img src="Foto/Titolo_b.gif" width="300" height="110"></a>
        <nav class="cd-main-nav">

            <a href="" class="header__icon-bar">
                <span></span>
                <span></span>
                <span></span>
            </a>
            <!--serve se vogliamo fare la versione mobile introducendo le tre linee per aprire menu-->
            <ul class="header__menu animate cd-main-nav__list js-js-accedi-trigger">
                <li class="header__menu__item home"><a href="index.php">Home</a></li>
                <li class="header__menu__item chi"><a href="index.php#about">Chi siamo</a></li>
                <li class="header__menu__item feat"><a href="index.php#features">Come Iniziare</a></li>
                <li class="header__menu__item"><a href="Le_Tue_Piante.php">Le Tue Piante</a></li>
                <li class="header__menu__item"><a href="Forum.php">Forum</a></li>
                <li class="header__menu__item"><a href="Store.html">Shop</a></li>
                <?php if (!isset($_SESSION['nome'])){

                ?>
                <li class="header__menu__item">
                    <a class="cd-main-nav__item cd-main-nav__item--accedi" name="loginButton_piante" href="#0 " data-accedi="login">Accedi</a></li>
                   
                <li class="header__menu__item"> 
                    <a class="cd-main-nav__item cd-main-nav__item--signup" name="registrationButton_piante" href="#0 " data-accedi="signup">Registrati</a></li>
                <?php }
                if (isset($_SESSION['nome'])){
                    echo "<li class='header__menu__item scritta'>Ciao, ".$_SESSION['nome']."</li>";
                ?>
                <li class="header__menu__item">
                <a class="cd-main-nav__item cd-main-nav__item--accedi" href="Logout.php">Esci</a>
                </li>
                <?php 
                }?>
            </ul>
        </nav>
    </header>


    <!--//////////LOGIN FORM///////////-->
    <div class="form js-js-accedi ">
        <!-- Form incluso il background -->
        <div class="form__container ">
            <!-- Container wrapper -->
            <ul class="form__switcher js-js-accedi-switcher js-js-accedi-trigger">
                <li><a href="#0 " data-accedi="login" data-type="login">Accedi</a></li>
                <li><a href="#0 " data-accedi="signup" data-type="signup">Registrati</a></li>
            </ul>

            <div class="form__block js-js-accedi-block" data-type="login">
                <!-- Login form -->
                <form class="form__form" action="login.php" method="post">
                    <img src="Foto/Logo.jpg" width="550">
                    <p class="form__fieldset">
                        <label class="form__label" for="accedi-email">E-mail</label>
                        <input class="form__input form__input--full-width form__input--has-padding form__input--has-border" id="accedi-email" type="email" placeholder="E-mail" name="inputEmail" maxlength="40" required>
                    </p>

                    <p class="form__fieldset ">
                        <label class="form__label form__label--password " for="accedi-password ">Password</label>
                        <input class="form__input form__input--full-width form__input--has-padding form__input--has-border" id="accedi-password" type="password" placeholder="Password" maxlength="8" name="inputPassword" required>
                        <a href="#0 " class="form__hide-password js-hide-password">Mostra</a>
                    </p>

                    <!--<p class="form__fieldset">
                        <label for="remember-me">Ricordami</label>
                        <input type="checkbox" id="remember-me " checked class="form__input">

                    </p>-->

                    <p class="form__fieldset">
                        <input class="form__input form__input--full-width" name="loginButton_piante" type="submit" value="Login">
                    </p>
                    <p class="form__bottom-message js-js-accedi-trigger"><a href="#0 " data-accedi="reset">Password dimenticata?</a></p>
                </form>


            </div>
            <!-- form__block -->

            <div class="form__block js-js-accedi-block" data-type="signup">
                <!-- Registrati form -->
                <form  action="validateRegistration.php" class="form__form" method="POST" name="myForm" >
                    <p class="form__fieldset">
                        <label class="form__label form__label--username" for="signup-username ">Username</label>
                        <input class="form__input form__input--full-width form__input--has-padding form__input--has-border" id="signup-username" name="inputUsername" type="text" placeholder="Username" maxlength="40" required>
                    </p>

                    <p class="form__fieldset ">
                        <label class="form__label form__label--email" for="signup-email">E-mail</label>
                        <input class="form__input form__input--full-width form__input--has-padding form__input--has-border" id="signup-email" name="inputEmail" type="email" placeholder="E-mail" maxlength="40" required>
                    </p>

                    <p class="form__fieldset">
                        <label class="form__label form__label--password " for="signup-password">Password</label>
                        <input class="form__input form__input--full-width form__input--has-padding form__input--has-border" id="signup-password" name="inputPassword" type="text" placeholder="Password" maxlength="8"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{3,}" required>
                        <a href="#0 " class="form__hide-password js-hide-password">Nascondi</a>
                    </p>


                    <p class="form__fieldset">
                        <input type="submit" class="form__input form__input--full-width form__input--has-padding" name="registrationButton_piante" value="Crea Account">
                    </p>
                    <div id="message">
                        <h5>La password deve contenere:</h5>
                        <p id="letter" class="invalid">Una <b>lettera</b> minuscola</p>
                        <p id="capital" class="invalid">Una <b>lettera</b> maiuscola</p>
                        <p id="number" class="invalid">Un <b>numero</b></p>
                    </div>
                </form>
            </div>
            <!-- form__block -->

            <div class="form__block js-js-accedi-block" data-type="reset">
                <!-- Reset password form -->
                <p class="form__message">Hai dimenticato la password? </p>
                <p class="form__message">Inserisci la tua mail e riceverai il link per generarne una nuova.</p>

                <form class="form__form">
                    <p class="form__fieldset">
                        <label class="form__label form__label--email" for="reset-email">E-mail</label>
                        <input class="form__input form__input--full-width form__input--has-padding form__input--has-border" id="reset-email" type="email" placeholder="E-mail" maxlength="40">
                    </p>

                    <p class="form__fieldset">
                        <input class="form__input form__input--full-width form__input--has-padding" type="submit" value="Reset password">
                    </p>

                    <p class="form__bottom-message js-js-accedi-trigger"><a href="#0" data-accedi="login">Torna al log-in</a></p>
                </form>
            </div>
            <!-- form__block -->

        </div>
        <!-- form__container -->
    </div>
    <!--/////////////////FINE LOGIN//////////////-->

    <div>
        <div class="foto_piante text-center"> 
            <!--il div serve per posizionare foto di sfondo-->
            <h1>Le Tue Piante</h1>
         <h2>da comprare una volta ed usare per sempre</h2>
        </div>
    </div>
    <!-- Features -->

    <!-- /Features -->

    <section>
        <!--Inizio Banner-->
        <div class="banner clearfix ">
            <!--<div class="banner__image ">img</div>-->

            <div class="conta">
                <h2>Ti manca qualcosa?</h2>
                <p>Hai finito la terra, il concime? Vuoi comprare un vaso o una pianta che non hai?</p>
                <a class="btnn btnn-ghost " href="Store.html">Vai allo Store</a>

            </div>

        </div>
    </section>


    <!--Sezione immagini l'ho fatta simile a quella sul tutorial che mi hai mandato ma il testo secondo 
        me è meglio farlo apprire quando ci passi sopra con il mouse-->
    <section class="clearfix text-center">
        <div class="frutti">
             
            <h2 class="text color_blue">
                Le Tue Piante
            </h2>
            <?php if (!isset($_SESSION['nome'])){
                echo "<p>Devi effettuare il Login prima di procedere</p>";
            }
            ?>
        <!--</div>-->
       
            
           <?php            
                if (isset($_SESSION['nome'])){
                    $dbconn=pg_connect("host=localhost port=5432 dbname=postgres user=postgres password=ulivo") or die ('Could connect: ' . pg_last_error());
                    $nome=$_SESSION['nome'];
                    $q1="SELECT p.nome, p.pianta, p.n_sci, p.n_ami, p.data, p.storia FROM piante p  WHERE p.nome='$nome'"; //JOIN organizzazione o ON p.nome=o.nome
                    $result=pg_query($q1);
                    if(pg_num_rows($result)!=0)  echo "<p> Vedo che simpatici amici si aggirano per casa tua! </p>";
                    else echo "<p> Non hai aggiunto ancora nessuna pianta. Cosa aspetti? </p>";
                    echo "<div class='divider'><a class='btn_p' href='Aggiungi_pianta.php'>Aggiungi Pianta</a></div>";
                    ?>
                    </div>
                <div class="container">
                    <?php
                     while($line=pg_fetch_array($result, null, PGSQL_ASSOC)){
                         $user=$line['nome'];
                         $tipo=$line['pianta'];
                         $n_sci=$line['n_sci'];
                         $n_ami=$line['n_ami'];
                         $data=$line['data'];
                         $data = date("d-m-Y", strtotime($data)); //di default è yyyy-mm-gg 
                         $storia=$line['storia'];
                         if($tipo=='Carota'){
             ?>
           
            
            <div class="row">
                
                    <div class="card left_margin">
                            <img class="card_image" src="Foto/Personaggi/Carota.jpg " alt="Carota ">
                            <span class="caption fade-caption">
                                <h2><?php echo $n_ami?></h2>
                            </span>
                    </div>
      
    
                 <div class="bubble bubble-bottom-left_carota color-carota" >Ciao sono una pianta di <?php echo $tipo?>, di <?php echo $user?>! Il mio vero nome è
                <?php echo $n_sci ?> ma puoi chiamarmi <?php echo $n_ami?>. La mia data di nascita è <?php echo $data?>. E la mia storia è questa..."<?php echo $storia ?>".</div>
               
                <div class="feature-col">
                    <div class="cella  text-center color-bianco-antico">
                        <div >
                            <div class="feature-icon pointer" name="ajax" value="carota">
                                <span class="fa fa-plus"  value="carota"></span>
                            </div>
                        </div> 
                    </div>
                </div>

                <div class="feature-col">
                    <div class="cella  text-center color-bianco-antico">
                        <div>
                            <a href="rimuovi_Pianta.php?pianta=Carota" onclick="return confirm('Procedendo eliminerai la pianta definitivamente. Continuare?')">
                                <div class="feature-icon">
                                    <span class="fa fa-skull"></span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
                      
            <span id="zonaDinamica_carota" class="zona" ></span>
            <?php
                }

                if($tipo=='Ananas'){

            ?>

            <div class="row">
                <div class="card left_margin">
                    <img class="card_image " src="Foto/Personaggi/Ananas.jpg " alt="Ananas ">
                    <span class="caption fade-caption">
                        <h2><?php echo $n_ami?></h2>
                        
                    </span>
                </div>
                <div class="bubble bubble-bottom-left_ananas color-ananas" >Ciao sono una pianta di <?php echo $tipo?>, di <?php echo $user?>! Il mio vero nome è
                <?php echo $n_sci ?> ma puoi chiamarmi <?php echo $n_ami?>. La mia data di nascita è <?php echo $data?>. E la mia storia è questa..."<?php echo $storia ?>".
                </div>
                <div class="feature-col">
                    <div class="cella  text-center color-bianco-antico">
                        <div>
                            <div class="feature-icon pointer" name="ajax" value="ananas">
                                <span class="fa fa-plus"  value="ananas"></span> <!--name="ajax" value="ananas"-->
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="feature-col">
                    <div class="cella  text-center color-bianco-antico">
                        <div><a href="rimuovi_Pianta.php?pianta=Ananas" onclick="return confirm('Procedendo eliminerai la pianta definitivamente. Continuare?')">
                                <div class="feature-icon">
                                    <span class="fa fa-skull"></span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <span id="zonaDinamica_ananas" class="zona" ></span>

            <?php
                }

                if($tipo=='Pomodoro'){

                
              ?>
        
            <div class="row">
                <div class="card left_margin">
                    <img class="card_image " src="Foto/Personaggi/Pomodoro.jpg " alt="Pomodoro ">
                    <span class="caption fade-caption">
                        <h2><?php echo $n_ami?></h2>
            
                    </span>
                </div>
                <div class="bubble bubble-bottom-left_pomodoro color-pomodoro" >Ciao sono una pianta di <?php echo $tipo?>, di <?php echo $user?>! Il mio vero nome è
                <?php echo $n_sci ?> ma puoi chiamarmi <?php echo $n_ami?>. La mia data di nascita è <?php echo $data?>. E la mia storia è questa..."<?php echo $storia ?>".</div>
                <div class="feature-col">
                    <div class="cella  text-center color-bianco-antico">
                        <div>
                            <div class="feature-icon pointer" name="ajax" value="pomodoro">
                                <span class="fa fa-plus" value="pomodoro"></span>
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="feature-col">
                    <div class="cella  text-center color-bianco-antico">
                        <div><a href="rimuovi_Pianta.php?pianta=Pomodoro" onclick="return confirm('Procedendo eliminerai la pianta definitivamente. Continuare?')">
                                <div class="feature-icon">
                                    <span class="fa fa-skull"></span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <span id="zonaDinamica_pomodoro" class="zona" ></span>
            
            <?php
                }

                if($tipo=='Lattuga'){ 
            
            ?>


            <div class="row">
                <div class="card left_margin">
                    <img class="card_image " src="Foto/Personaggi/Lattuga.jpg " alt="Lattuga ">
                    <span class="caption fade-caption">
                        <h2><?php echo $n_ami?></h2>
                       
                    </span>
                </div>
                <div class="bubble bubble-bottom-left_lattuga color-lattuga" >Ciao sono una pianta di <?php echo $tipo?>, di <?php echo $user?>! Il mio vero nome è
                <?php echo $n_sci ?> ma puoi chiamarmi <?php echo $n_ami?>. La mia data di nascita è <?php echo $data?>. E la mia storia è questa..."<?php echo $storia ?>".</div>
                <div class="feature-col">
                    <div class="cella  text-center color-bianco-antico">
                        <div>
                            <div class="feature-icon pointer" name="ajax" value="lattuga">
                                <span class="fa fa-plus"  value="lattuga"></span>
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="feature-col">
                    <div class="cella  text-center color-bianco-antico">
                        <div><a href="rimuovi_Pianta.php?pianta=Lattuga" onclick="return confirm('Procedendo eliminerai la pianta definitivamente. Continuare?')">
                                <div class="feature-icon">
                                    <span class="fa fa-skull"></span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <span id="zonaDinamica_lattuga" class="zona" ></span>

            <?php
                }

                if($tipo=='Avocado'){ 
              ?>


            <div class="row">
                <div class="card left_margin">
                    <img class="card_image " src="Foto/Personaggi/Avocado.jpg " alt="Avocado ">
                    <span class="caption fade-caption">
                        <h2><?php echo $n_ami?></h2>
                        
                    </span>
                </div>
                <div class="bubble bubble-bottom-left_avocado color-avocado" >Ciao sono una pianta di <?php echo $tipo?>, di <?php echo $user?>! Il mio vero nome è
                <?php echo $n_sci ?> ma puoi chiamarmi <?php echo $n_ami?>. La mia data di nascita è <?php echo $data?>. E la mia storia è questa..."<?php echo $storia ?>".</div>
                <div class="feature-col">
                    <div class="cella  text-center color-bianco-antico">
                        <div>
                            <div class="feature-icon pointer" name="ajax" value="avocado">
                                <span class="fa fa-plus"  value="avocado"></span>
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="feature-col">
                    <div class="cella  text-center color-bianco-antico">
                        <div><a href="rimuovi_Pianta.php?pianta=Avocado" onclick="return confirm('Procedendo eliminerai la pianta definitivamente. Continuare?')">
                                <div class="feature-icon">
                                    <span class="fa fa-skull"></span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <span id="zonaDinamica_avocado" class="zona" ></span>
            <?php
                }

                if($tipo=='Cipolla'){ 

            ?>



            <div class="row">
                <div class="card left_margin">
                    <img class="card_image " src="Foto/Personaggi/Cipolla.jpg " alt="Cipolla ">
                    <span class="caption fade-caption">
                        <h2><?php echo $n_ami?></h2>
                      
                    </span>
                </div>
                <div class="bubble bubble-bottom-left_cipolla color-cipolla" >Ciao sono una pianta di <?php echo $tipo?>, di <?php echo $user?>! Il mio vero nome è
                <?php echo $n_sci ?> ma puoi chiamarmi <?php echo $n_ami?>. La mia data di nascita è <?php echo $data?>. E la mia storia è questa..."<?php echo $storia ?>".</div>
                <div class="feature-col">
                    <div class="cella  text-center color-bianco-antico">
                        <div>
                            <div class="feature-icon pointer" name="ajax" value="cipolla">
                                <span class="fa fa-plus"  value="cipolla"></span>
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="feature-col">
                    <div class="cella  text-center color-bianco-antico">
                        <div><a href="rimuovi_Pianta.php?pianta=Cipolla" onclick="return confirm('Procedendo eliminerai la pianta definitivamente. Continuare?')">
                                <div class="feature-icon">
                                    <span class="fa fa-skull"></span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <span id="zonaDinamica_cipolla" class="zona" ></span>
            
            <?php
                }

                if($tipo=='Banana'){ 

            ?>

            <div class="row">
                <div class="card left_margin">
                    <img class="card_image " src="Foto/Personaggi/Banana.jpg " alt="Banana ">
                    <span class="caption fade-caption">
                        <h2><?php echo $n_ami?></h2>
                        
                    </span>
                </div>
                <div class="bubble bubble-bottom-left_banana color-banana" >Ciao sono una pianta di <?php echo $tipo?>, di <?php echo $user?>! Il mio vero nome è
                <?php echo $n_sci ?> ma puoi chiamarmi <?php echo $n_ami?>. La mia data di nascita è <?php echo $data?>. E la mia storia è questa..."<?php echo $storia ?>".</div>
                <div class="feature-col">
                    <div class="cella  text-center color-bianco-antico">
                        <div>
                            <div class="feature-icon pointer" name="ajax" value="banana">
                                <span class="fa fa-plus"  value="banana"></span>
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="feature-col">
                    <div class="cella  text-center color-bianco-antico">
                        <div><a href="rimuovi_Pianta.php?pianta=Banana" onclick="return confirm('Procedendo eliminerai la pianta definitivamente. Continuare?')">
                                <div class="feature-icon">
                                    <span class="fa fa-skull"></span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <span id="zonaDinamica_banana" class="zona" ></span>
           

            <?php
                }

                if($tipo=='Patata'){ 

            ?>


            <div class="row">
                <div class="card left_margin">
                    <img class="card_image " src="Foto/Personaggi/Patata.jpg " alt="Patata ">
                    <span class="caption fade-caption">
                        <h2><?php echo $n_ami?></h2>
                    
                    </span>
                </div>
                <div class="bubble bubble-bottom-left_patata color-patata" >Ciao sono una pianta di <?php echo $tipo?>, di <?php echo $user?>! Il mio vero nome è
                <?php echo $n_sci ?> ma puoi chiamarmi <?php echo $n_ami?>. La mia data di nascita è <?php echo $data?>. E la mia storia è questa..."<?php echo $storia ?>".</div>
                <div class="feature-col">
                    <div class="cella  text-center color-bianco-antico">
                        <div>
                            <div class="feature-icon pointer" name="ajax" value="patata">
                                <span class="fa fa-plus"  value="patata"></span> 
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="feature-col">
                    <div class="cella  text-center color-bianco-antico">
                        <div><a href="rimuovi_Pianta.php?pianta=Patata" onclick="return confirm('Procedendo eliminerai la pianta definitivamente. Continuare?')">
                                <div class="feature-icon">
                                    <span class="fa fa-skull"></span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            
            <span id="zonaDinamica_patata" class="zona" ></span>
       
        <?php
            }
        }
        } ?>
    </div>
    </container>
    </section>
    <!--Fine delle cards-->
  
        <script>
            var documenti = document.getElementsByName("ajax");
            for (var i = 0; i < documenti.length; i++) {
                documenti[i].onclick = caricaPagina;
            }

            function caricaPagina(e) {
                var pianta_chiamante=e.target.getAttribute("value");
                if (document.getElementById("zonaDinamica_"+pianta_chiamante).innerHTML==""){
                e.preventDefault();
                var httpRequest = new XMLHttpRequest();
                httpRequest.prevTarget = e.target;
                httpRequest.onreadystatechange = gestisciResponse;
                //httpRequest.open("GET", "Dinamiche/"+pianta_chiamante+".html", true); 
                httpRequest.open("GET", "Linea_temp/"+pianta_chiamante+".html", true); 
                httpRequest.send();
                }
                else {
                    e.preventDefault();
                    document.getElementById("zonaDinamica_"+pianta_chiamante).innerHTML="";
                }
            }

            function gestisciResponse(e) {
                if (e.target.readyState == 4 && e.target.status == 200) {
                    document.getElementById("zonaDinamica_"+e.target.prevTarget.getAttribute("value")).innerHTML =
                        e.target.responseText;
                }
            }
        </script>
    
    <!--Footer-->
    <footer class="footer">
        <div class="row">
            <div class="col-lg-6 col-xs-12 text-lg-left text-center">
                <p class="end-text">
                    “To plant a garden is to believe in tomorrow.” H.A.
                </p>
            </div>
            <div class="col-lg-6 col-xs-12 text-lg-right text-center">
                <ul class="list-inline">
                    <li class="list-inline-item">
                        <a href="index.php">Home</a>
                    </li>

                    <li class="list-inline-item">
                        <a href="index.php#about">Chi Siamo</a>
                    </li>

                    <li class="list-inline-item">
                        <a href="index.php#features">Come Iniziare</a>
                    </li>

                    <li class="list-inline-item">
                        <a href="">Le Tue Piante</a>
                    </li>

                    <li class="list-inline-item">
                        <a href="Forum.php">Forum</a>
                    </li>

                    <li class="list-inline-item">
                        <a href="Store.html">Shop</a>
                    </li>
                </ul>
            </div>
        </div>
    </footer>
    <!--Fine footer-->

    <a class="scrolltop" href="#" id="myTop"><span class="fa fa-angle-up fa fa-angle-up"></span></a>
    <!-- Resource JavaScript -->
    <script type="text/javascript" lang="javascript" src="lib/main.js "></script>
    <script type="text/javascript" lang="javascript" src="lib/jquery/jquery.min.js "></script>
    <!--Librerie necessarie per il counter-->
    <script type="text/javascript" lang="javascript" src="lib/counterup/counterup.min.js"></script>
    <script type="text/javascript" lang="javascript" src="lib/waypoints/waypoints.min.js"></script>
    <script type="text/javascript" lang="javascript" src="lib/jquery/jquery-migrate.min.js"></script>
    <!--Qui ci sono gli script JQuery e la funzione onready-->
    <script type="text/javascript" lang="javascript" src="lib/custom.js"></script>
    <!--Importa la libreria per bloccare il menu e usa una funzione in custom.js che setta lo spazio in alto =0-->
    <script type="text/javascript" lang="javascript" src="lib/stickyjs/sticky.js"></script>
    <!--Sono le icone smile delle piante-->
   <script src="https://kit.fontawesome.com/cc496d5143.js" crossorigin="anonymous"></script>
</body>

</html>