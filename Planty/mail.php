<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require $_SERVER['DOCUMENT_ROOT'] . '/mail/Exception.php';
require $_SERVER['DOCUMENT_ROOT'] . '/mail/PHPMailer.php';
require $_SERVER['DOCUMENT_ROOT'] . '/mail/SMTP.php';

$utente=$_POST['utente'];
$email=$_POST['email'];
$messaggio=$_POST['messaggio'];

$mail = new PHPMailer;
$mail->isSMTP(); 
$mail->SMTPDebug = 0; // 0 = commenti off (for production use) - 1 = solo client - 2 = client e server 
$mail->Host = "smtp.gmail.com"; 
$mail->Port = 587; // TLS 
$mail->SMTPSecure = 'tls'; // ssl is deprecated
$mail->SMTPAuth = true;
$mail->Username = 'planty.assistenza@gmail.com'; // email
$mail->Password = 'planty2020'; // password
$mail->setFrom("$email", "$utente"); //da
$mail->addAddress('sa.pieri.98@gmail.com'); // to email puoi passargli anche il nome
$mail->Subject = 'Assistenza';
$mail->msgHTML("$messaggio");
$mail->AltBody = 'HTML messaging not supported'; // Se le html non sono supportate dal ricevente
$mail->addAttachment('Foto/Logo.jpg'); //Logo nostro
$mail->SMTPOptions = array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                );
if(!$mail->send()){
    echo "Mailer Error: " . $mail->ErrorInfo;
}else{
   //echo "Message sent!";
   echo "<script>window.location='Store.html';</script>";
}
