**Welcome to Plant-y.it !**  
_Plants to buy once and grow forever_


🌱 This repository contains all the material used to build from scratch the website: www.plant-y.it

🌱 The purpose of the website is to offer the possibility of reusing food waste, to revive plants to grow at home!

🌱 After registration and login, you will find all the information to grow your seedlings.


<img src="./Planty/Foto/Gruppo.png"  heigh="200" >




- Since the website was created for educational purposes, it also contains a forum to exchange experiences and advice and a demo store.  
- All the graphics contained within the site were made by me :).
- The website is in Italian, english version coming soon!

> _We can all do our part for a greener world!_
